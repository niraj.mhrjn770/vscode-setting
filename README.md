"workbench.colorCustomizations": {
    "editorCursor.foreground": "#cdcdcd",
    "terminalCursor.foreground": "#c0c0c0",
    "terminal.foreground": "#efefef",
    "[GitHub Dark]": {
      "terminal.background": "#24292e",
      "sideBar.background": "#24292e",
      "sideBar.foreground": "#ccc",
      // "editor.selectionBackground": "#979797",
      "menu.background": "#24292e",
      "menu.foreground": "#ccc",
      "activityBar.foreground": "#dedede",
      // Git Colors
      "gitDecoration.modifiedResourceForeground": "#ffb250",
      "gitDecoration.deletedResourceForeground": "#c40570",
      "gitDecoration.untrackedResourceForeground": "#00fcff",
      "gitDecoration.ignoredResourceForeground": "#5c5c5c",
      "gitDecoration.conflictingResourceForeground": "#b50009"
    },
  }

  "editor.semanticTokenColorCustomizations": {
    "[Monokai]": {
      "enabled": true,
      "rules": {
        "class": {
          "foreground": "#7efed6",
          "fontStyle": "",
        },
        "namespace": {
          "fontStyle": "",
        },
      },
    },
    "[Dark Mode MDN]": {
      "enabled": true,
      "rules": {
        "class": {
          "foreground": "#ffdf8d",
        }
      }
    },
    "[GitHub Dark]": {
      "enabled": true,
      "rules": {
        "class": {
          "foreground": "#ace0d7",
        }
      }
    }
  },
  "editor.tokenColorCustomizations": {
    "[Monokai]": {
      "variables": "#fcfcfc",
      "numbers": "#ffc379",
      "comments": {
        "foreground": "#707070",
        "fontStyle": "italic",
      },
      // "keywords": "#ffaf76",
      "strings": "#e8ff66",
      "functions": {
        "foreground": "#ffb0ff",
        "fontStyle": "italic",
      },
    },
    "[GitHub Dark]": {
      "variables": "#ececec",
      "numbers": "#f8b9ff",
      "comments": {
        "foreground": "#6b6b6b",
        "fontStyle": "italic",
      },
      "keywords": "#ffafd1",
      "strings": "#5affad",
      "functions": {
        "foreground": "#a2f7ff",
        "fontStyle": "italic",
      }
    },
    "[GitHub Dark Dimmed]": {
      "variables": "#ccc",
      "numbers": "#fc50ad",
      "comments": {
        "foreground": "#666666",
        "fontStyle": "italic",
      },
      "keywords": "#63bee3",
      "strings": "#5affad",
      "functions": {
        "foreground": "#f09ccc",
        "fontStyle": "italic",
      }
    }
  }